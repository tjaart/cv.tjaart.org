module.exports = (icon) => {
  return icon ? `/assets/img/${icon}.svg` : "";
};

