module.exports = {
  title: "Computer Vision",
  description: "Tjaart's Computer Vision Portfolio",
  keywords: ['computer vision', 'opencv', 'pytorch'],
  url: 'https://portfolio.tjaart.dev', // your site url without trailing slash
  paginate: 100 // how many posts you want to show for each page
  // uncomment the next line if you want to add disqus to your site
  // disqusShortname: "your-shortname"
};
