---
title: "Ball Tracking"
description: ""
date: 2024-03-15T21:43
icons: 
    - opencv
thumb: "ball_tracker.png"
repo_url: https://gitlab.com/tjaart/ball_tracker
tags: 
    - OpenCV
---

For this ball tracking algorithm, we detect the ball using a YoloV3 detector, and then track the ball using a Kernelized Correlation Filter tracker. Every tenth frame, we do a new detection, to ensure that the tracking does not drift.


<iframe width="360" height="640" src="https://www.youtube.com/embed/JGqXwAZ8Mcg" title="Ball tracking" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
