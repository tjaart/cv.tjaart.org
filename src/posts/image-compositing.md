---
title: "Image Compositing for Photographic Lighting"
date: 2015-05-15T21:43
icons: 
    - opencv
thumb: "image_compositing.png"
repo_url: https://github.com/tjaartvdwalt/light-compositing
tags: 
    - OpenCV
    - SVM
---
This project formed part of my MS degree. It implements of the ideas found in the paper [User-assisted Image Compositing for Photographic Lighting](https://www.cs.cornell.edu/projects/light_compositing/) by Ivaylo Boyadzhiev, Sylvain Paris and Kavita Bala.

The code is availble [here](https://github.com/tjaartvdwalt/light-compositing), and full results are given in the [project report](https://raw.githubusercontent.com/tjaartvdwalt/light-compositing/master/docs/light_composition_report.pdf).

The basic idea


<div class="flex flex-wrap">
  <img style="max-width: 300px" src="/assets/img/cafe_1.png">
  <img style="max-width: 300px" class="ml-2" src="/assets/img/cafe_2.png">
  <img style="max-width: 300px" class="ml-2" src="/assets/img/cafe_3.png">
</div>
<div>Representative images from the cafe dataset</div>

<div class="flex flex-wrap">
  <img style="max-width: 300px" src="/assets/img/fruit_basket_1.png">
  <img style="max-width: 300px" class="ml-2" src="/assets/img/fruit_basket_2.png">
  <img style="max-width: 300px" class="ml-2" src="/assets/img/fruit_basket_3.png">
</div>
<div>Representative images from the fruit basket dataset</div>


# Basis Lights

## Fill light

Provides even illumination across the image

<div class="flex flex-wrap">
  <figure>
    <img style="max-width: 300px" src="/assets/img/cafe_avg_light.png">
    <figcaption>Average light</figcaption>
  </figure>
  <figure class="ml-2">  
    <img style="max-width: 300px" src="/assets/img/cafe_fill_light.png">
    <figcaption>Fill light</figcaption>
  </figure>
</div>

<div class="flex flex-wrap">
  <figure>
    <img style="max-width: 300px" src="/assets/img/fruit_basket_avg_light.png">
    <figcaption>Average light</figcaption>
  </figure>
  <figure class="ml-2">  
    <img style="max-width: 300px" src="/assets/img/fruit_basket_fill_light.png">
    <figcaption>Fill light</figcaption>
  </figure>
</div>

## Edge light

Emphasizes the main edges in the scene

<div class="flex flex-wrap">
  <figure>
    <img style="max-width: 300px" src="/assets/img/cafe_avg_light.png">
    <figcaption>Average light</figcaption>
  </figure>
  <figure class="ml-2">
    <img style="max-width: 300px" src="/assets/img/cafe_edge_light.png">
    <figcaption>Edge light</figcaption>
  </figure>
</div>

## Diffuse color light

Emphasizes the base colors of objects

<div class="flex flex-wrap">
  <figure>
    <img style="max-width: 300px" src="/assets/img/cafe_avg_light.png">
    <figcaption>Average light</figcaption>
  </figure>
  <figure class="ml-2">
    <img style="max-width: 300px" src="/assets/img/cafe_diffuse_light.png">
    <figcaption>Diffuse color light</figcaption>
  </figure>
</div>

<div class="flex flex-wrap">
  <figure>
    <img style="max-width: 300px" src="/assets/img/fruit_basket_avg_light.png">
    <figcaption>Average light</figcaption>
  </figure>
  <figure class="ml-2">
    <img style="max-width: 300px" src="/assets/img/fruit_basket_diffuse_light.png">
    <figcaption>Diffuse color light</figcaption>
  </figure>
</div>
# Light modifiers

## Soft light modifier

Removes harsh shadows and highlights


<div class="flex flex-wrap">
  <figure>
    <img style="max-width: 300px" src="/assets/img/cafe_avg_light.png">
    <figcaption>Average light</figcaption>
  </figure>
  <figure class="ml-2">
    <img style="max-width: 300px" src="/assets/img/cafe_soft_light_modifier.png">
    <figcaption>Soft light modifier</figcaption>
  </figure>
</div>


<div class="flex flex-wrap">
  <figure>
    <img style="max-width: 300px" src="/assets/img/fruit_basket_avg_light.png">
    <figcaption>Average light</figcaption>
  </figure>
  <figure class="ml-2">
    <img style="max-width: 300px" src="/assets/img/fruit_basket_soft_light_modifier.png">
    <figcaption>Soft light modifier</figcaption>
  </figure>
</div>

## Regional lighting modifier
Balances the overall lighting of the scene


<div class="flex flex-wrap">
  <figure>
    <img style="max-width: 300px" src="/assets/img/cafe_avg_light.png">
    <figcaption>Average light</figcaption>
  </figure>
  <figure class="ml-2">
    <img style="max-width: 300px" src="/assets/img/cafe_regional_light_modifier.png">
    <figcaption>Regional light modifier applied to the fill light image</figcaption>
  </figure>
</div>
